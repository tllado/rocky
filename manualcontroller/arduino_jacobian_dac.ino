#include "Wire.h"

#define dac_address 0x28

const float pi = 3.141592653589;

const int xPin = A0;
const int yPin = A1;
const int zPin = A2;
const int aPin = 3;
const int bPin = 5;
const int cPin = 6;

const float xZero = 2.55;
const float yZero = 2.5;
const float zZero = 2.61;
const float xRange = 4.3 - 0.88;
const float yRange = 4.25 - 0.75;
const float zRange = 4.4 - 0.4;
const float noise = 0.2;

const float thetaOffset = 270*pi/180;
const float thetaA = 0*pi/180 + thetaOffset;
const float thetaB = 120*pi/180 + thetaOffset;
const float thetaC = 240*pi/180 + thetaOffset;
const float r = 1.0;

float xVolt = 0.0;
float yVolt = 0.0;
float zVolt = 0.0;
float xDot = 0.0;
float yDot = 0.0;
float zDot = 0.0;

float aDot = 0.0;
float bDot = 0.0;
float cDot = 0.0;
byte dac0 = 0x00;
byte dac1 = 0x01;
byte dac2 = 0x02;
byte dac3 = 0x03;
byte middle = 0x74;
byte aSig = 0x00;
byte bSig = 0x00;
byte cSig = 0x00;

void setup() {
  Wire.begin();
  pinMode(aPin, OUTPUT);
  pinMode(bPin, OUTPUT);
  pinMode(cPin, OUTPUT);
}

void loop() {
  xVolt = analogRead(xPin)/1024.0*5.0;
  yVolt = analogRead(yPin)/1024.0*5.0;
  zVolt = analogRead(zPin)/1024.0*5.0;

  xDot = (xVolt - xZero)/(xRange/2.0);
  yDot = (yVolt - yZero)/(yRange/2.0)*(-1.0);
  zDot = (zVolt - zZero)/(zRange/2.0);

  xDot = (abs(xDot) < noise)?0.0:xDot;
  yDot = (abs(yDot) < noise)?0.0:yDot;
  zDot = (abs(zDot) < noise)?0.0:zDot;

  aDot = xDot*sin(thetaA) + yDot*cos(thetaA) + zDot*r;
  bDot = xDot*sin(thetaB) + yDot*cos(thetaB) + zDot*r;
  cDot = xDot*sin(thetaC) + yDot*cos(thetaC) + zDot*r;

  if(max(abs(aDot), max(abs(bDot), abs(cDot))) > 1) {
    aDot = aDot/max(abs(aDot), max(abs(bDot), abs(cDot)));
    bDot = bDot/max(abs(aDot), max(abs(bDot), abs(cDot)));
    cDot = cDot/max(abs(aDot), max(abs(bDot), abs(cDot)));
  }

  aDot = aDot*abs(aDot);
  bDot = bDot*abs(bDot);
  cDot = cDot*abs(cDot);

  aSig = aDot*127 + 127;
  bSig = bDot*127 + 127;
  cSig = cDot*127 + 127;

  // Serial.println("debug");

  Wire.beginTransmission(dac_address);
  Wire.write(dac0);
  Wire.write(middle);
  Wire.write(dac1);
  Wire.write(aSig);
  Wire.write(dac2);
  Wire.write(bSig);
  Wire.write(dac3);
  Wire.write(cSig);
  Wire.endTransmission();
}
