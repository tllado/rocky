const int xPin = A0;
const int yPin = A1;
const int zPin = A2;
const int aPin = 3;
const int bPin = 5;
const int cPin = 6;

const float xZero = 2.55;
const float yZero = 2.5;
const float zZero = 2.61;
const float xRange = 4.3 - 0.88;
const float yRange = 4.25 - 0.75;
const float zRange = 4.4 - 0.4;
const float noise = 0.2;

const float pi = 3.141592653589;

const float thetaOffset = 270*pi/180;
const float thetaA = 0*pi/180 + thetaOffset;
const float thetaB = 120*pi/180 + thetaOffset;
const float thetaC = 240*pi/180 + thetaOffset;
const float r = 1.0;

float xVolt;
float yVolt;
float zVolt;
float xDot;
float yDot;
float zDot;

float aDot;
float bDot;
float cDot;
int aPWM;
int bPWM;
int cPWM;

void setup() {
  pinMode(aPin, OUTPUT);
  pinMode(bPin, OUTPUT);
  pinMode(cPin, OUTPUT);
}

void loop() {
  xVolt = analogRead(xPin)/1024.0*5.0;
  yVolt = analogRead(yPin)/1024.0*5.0;
  zVolt = analogRead(zPin)/1024.0*5.0;

  xDot = (xVolt - xZero)/(xRange/2.0);
  yDot = (yVolt - yZero)/(yRange/2.0)*(-1.0);
  zDot = (zVolt - zZero)/(zRange/2.0);

  xDot = (abs(xDot) < noise)?0.0:xDot;
  yDot = (abs(yDot) < noise)?0.0:yDot;
  zDot = (abs(zDot) < noise)?0.0:zDot;

  aDot = xDot*sin(thetaA) + yDot*cos(thetaA) + zDot*r;
  bDot = xDot*sin(thetaB) + yDot*cos(thetaB) + zDot*r;
  cDot = xDot*sin(thetaC) + yDot*cos(thetaC) + zDot*r;

  if(max(abs(aDot), max(abs(bDot), abs(cDot))) > 1) {
    aDot = aDot/max(abs(aDot), max(abs(bDot), abs(cDot)));
    bDot = bDot/max(abs(aDot), max(abs(bDot), abs(cDot)));
    cDot = cDot/max(abs(aDot), max(abs(bDot), abs(cDot)));
  }

  aDot = aDot*abs(aDot);
  bDot = bDot*abs(bDot);
  cDot = cDot*abs(cDot);

  aPWM = aDot*127 + 127;
  bPWM = bDot*127 + 127;
  cPWM = cDot*127 + 127;

  // Serial.println("debug");

  analogWrite(aPin, aPWM);
  analogWrite(bPin, bPWM);
  analogWrite(cPin, cPWM);
}
