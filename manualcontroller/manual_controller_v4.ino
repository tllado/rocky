#include "SPI.h"

const float pi = 3.141592653589;

const int xPin = A0;
const int yPin = A1;
const int zPin = A2;
const int SyncPin = 2;

float xZero = 2.5;
float yZero = 2.5;
float zZero = 2.5;
const float xRange = 4.5 - 0.5;
const float yRange = 4.5 - 0.5;
const float zRange = 4.5 - 0.5;
const float noise = 0.2;

const float thetaOffset = 330*pi/180;
const float thetaA = 0*pi/180 + thetaOffset;
const float thetaB = 120*pi/180 + thetaOffset;
const float thetaC = 240*pi/180 + thetaOffset;
const float r = 1.0;

float xVolt = 0.0;
float yVolt = 0.0;
float zVolt = 0.0;
float xDot = 0.0;
float yDot = 0.0;
float zDot = 0.0;

float aDot = 0.0;
float bDot = 0.0;
float cDot = 0.0;
float scale = 0.0;
unsigned int aVal = 0;
unsigned int bVal = 0;
unsigned int cVal = 0;
const byte aReg = 0b00000000;
const byte bReg = 0b00000001;
const byte cReg = 0b00000010;

void setup()
{
  pinMode(SyncPin, OUTPUT);
  digitalWrite(SyncPin, HIGH);
  SPI.begin();
  SPI.setDataMode(SPI_MODE1);
  SPI.setBitOrder(MSBFIRST);
  WriteWord(0b00001100, 0b00000000, 0b00000100);  //Set Range
  WriteWord(0b00010000, 0b00000000, 0b00010111);  //Set Power On
  Serial.begin(115200);
  
  pinMode(xPin, INPUT);
  pinMode(yPin, INPUT);
  pinMode(zPin, INPUT);
  SetZero();
}

void loop()
{
  ReadCart();
  ScaleCart();
  CartToJoint();
  ScaleJoint();
  WriteJoint();
  delay(10);
}

void SetZero()
{
  xZero = analogRead(xPin)/1024.0*5.0;
  yZero = analogRead(yPin)/1024.0*5.0;
  zZero = analogRead(zPin)/1024.0*5.0;
}

void ReadCart()
{
  xVolt = analogRead(xPin)/1024.0*5.0;
  yVolt = analogRead(yPin)/1024.0*5.0;
  zVolt = analogRead(zPin)/1024.0*5.0;
 
  xDot = (xVolt - xZero)/(xRange/2.0)*(-1.0);
  yDot = (yVolt - yZero)/(yRange/2.0)*(-1.0);
  zDot = (zVolt - zZero)/(zRange/2.0)*(-1.0);
}

void ScaleCart()
{
  if(abs(xDot) < 0.1)
    xDot = 0;
  else
    xDot = (xDot - 0.1)*1.0/0.9;
  
  if(abs(yDot) < 0.1)
    yDot = 0;
  else
    yDot = (yDot - 0.1)*1.0/0.9;
  
  if(abs(zDot) < 0.1)
    zDot = 0;
  else
    zDot = (zDot - 0.1)*1.0/0.9;
}

void CartToJoint()
{
  xDot = (abs(xDot) < noise)?0.0:xDot;
  yDot = (abs(yDot) < noise)?0.0:yDot;
  zDot = (abs(zDot) < noise)?0.0:zDot;
  
  aDot = xDot*sin(thetaA) + yDot*cos(thetaA) + zDot*r;
  bDot = xDot*sin(thetaB) + yDot*cos(thetaB) + zDot*r;
  cDot = xDot*sin(thetaC) + yDot*cos(thetaC) + zDot*r;
}

void ScaleJoint()
{
  scale = max(abs(aDot), max(abs(bDot), abs(cDot)));
  
  if(scale > 1)
  {
    aDot = aDot/scale;
    bDot = bDot/scale;
    cDot = cDot/scale;
  }
  
  aDot = aDot*abs(aDot);
  bDot = bDot*abs(bDot);
  cDot = cDot*abs(cDot);
  
  aVal = aDot*32768 + 32768;
  bVal = bDot*32768 + 32768;
  cVal = cDot*32768 + 32768;
//  Serial.print(aVal);
//  Serial.print("\t");
//  Serial.print(bVal);
//  Serial.print("\t");
//  Serial.println(cVal);
}

void WriteJoint()
{
  WriteWord(aReg, aVal/256, aVal%256);
  WriteWord(bReg, bVal/256, bVal%256);
  WriteWord(cReg, cVal/256, cVal%256);
}

void WriteWord(byte Register, byte Value1, byte Value2)
{
  digitalWrite(SyncPin, LOW);
  SPI.transfer(Register);
  SPI.transfer(Value1);
  SPI.transfer(Value2);
  digitalWrite(SyncPin, HIGH);
}

//void ReadWord(byte Register)
//{
//  digitalWrite(SyncPin, LOW);
//  SPI.transfer(Register);
//  SPI.transfer(0x00);
//  SPI.transfer(0x00);
//  digitalWrite(SyncPin, HIGH);
//  digitalWrite(SyncPin, LOW);
//  byte bone = SPI.transfer(0x00);
//  byte btwo = SPI.transfer(0x00);
//  byte bthr = SPI.transfer(0x00);
//  digitalWrite(SyncPin, HIGH);
//  
//  Serial.print(bone);Serial.print(" ");
//  Serial.print(btwo);Serial.print(" ");
//  Serial.println(bthr);
//}
