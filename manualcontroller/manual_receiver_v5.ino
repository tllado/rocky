
// Rocky Manual Control Receiver, v5
// University of Texas at Austin
// Human Centered Robotics Laboratory
// Travis Llado, 2015.05.02
// 
// This program, written for a Sparkfun Arduino ProMicro,
// receives joint-space velocities in the form of DAC
// voltages from the remote control via an XBee v2 radio,
// then relays these values to Rocky's onboard DAC via SPI.
// This is the first wireless version of the program.

#include "SPI.h"
const int SyncPin = 10;

boolean DACIsOn = false;
unsigned int aVal = 0;
unsigned int bVal = 0;
unsigned int cVal = 0;
const byte aReg = 0b00000000;
const byte bReg = 0b00000001;
const byte cReg = 0b00000010;

const int BaudRate = 9600;
int LoopDelay = 0;  // milliseconds (ms)
const int ConnectedDelay = 25;  // ms
const int DisconnectedDelay = 500;  // ms

unsigned long TimedOut = 0;  // ms
const unsigned long MessageTimeout = 1000;  // ms

void setup()
{
  pinMode(SyncPin, OUTPUT);
  digitalWrite(SyncPin, HIGH);
  SPI.begin();
  SPI.setDataMode(SPI_MODE1);
  SPI.setBitOrder(MSBFIRST);
  Serial1.begin(BaudRate);
}

void loop()
{
  CheckForMessage();
  if (millis() < TimedOut)
  {
  	if (DACIsOn == false)
  	{
  		TurnDACOn();
  	}
  	UpdateDAC();
    Serial1.write('G');  //Send Receipt
    LoopDelay = ConnectedDelay;
  }
  else
  {
  	TurnDACOff();
  	Serial1.write('X');  //Send Receipt
  	LoopDelay = DisconnectedDelay;
  }
  delay(LoopDelay);
}





void CheckForMessage()
{
  char NewMessage = ' ';
  boolean aReceived = false;
  boolean bReceived = false;
  boolean cReceived = false;

  while (Serial1.available() > 0)
  {
    NewMessage = Serial1.read();

    if (NewMessage == 'A')
    {
      aVal = Serial1.read();
      aReceived = true;
    }
    else if (NewMessage == 'B')
    {
      bVal = Serial1.read();
      bReceived = true;
    }
    else if (NewMessage == 'C')
    {
      cVal = Serial1.read();
      cReceived = true;
    }
    else if (NewMessage == 'Z' || aReceived == true || bReceived == true || cReceived == true)
    {
      TimedOut = millis() + MessageTimeout;
    }
  }
}

void UpdateDAC()
{
  WriteWord(aReg, aVal/256, aVal%256);
  WriteWord(bReg, bVal/256, bVal%256);
  WriteWord(cReg, cVal/256, cVal%256);
}

void TurnDACOn()
{
  WriteWord(0b00011001, 0b00000000, 0b00000100);  //Set Clear Value
  WriteWord(0b00001100, 0b00000000, 0b00000100);  //Set Range to +/-10V
  WriteWord(0b00011100, 0b00000000, 0b00000000);  //Clear
  WriteWord(0b00010000, 0b00000000, 0b00010111);  //Set Power On
  DACIsOn = true;
  delay(1);
}

void TurnDACOff()
{
	WriteWord(0b00010000, 0b00000000, 0b00010000);  //Set Power On
	DACIsOn = false;
}

void WriteWord(byte Register, byte Value1, byte Value2)
{
  digitalWrite(SyncPin, LOW);
  SPI.transfer(Register);
  SPI.transfer(Value1);
  SPI.transfer(Value2);
  digitalWrite(SyncPin, HIGH);
}