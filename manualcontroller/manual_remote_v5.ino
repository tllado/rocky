// Rocky Manual Control Remote, v5
// University of Texas at Austin
// Human Centered Robotics Laboratory
// Travis Llado, 2015.05.02
// 
// This program, written for a Sparkfun Arduino Fio v3, reads
// two analog joysticks that provides cartesian velocities,
// converts these values to joint-space velocities, then
// transmits these values via an XBee v2 radio for Rocky's
// onboard DAC. This is the first wireless version of the
// program.

const float pi = 3.141592653589;

const int x1Pin = 23;
const int y1Pin = 22;
const int x2Pin = 21;
const int y2Pin = 20;
const int ConnectionLEDPin = 10;

const float BoardVoltage = 3.3;
const float AnalogResolution = 1024.0;
const float DACResolution = 65536.0;

float xZero = BoardVoltage/2.0;
float yZero = BoardVoltage/2.0;
float zZero = BoardVoltage/2.0;
const float xMax = 3.0;
const float xMin = 0.3;
const float yMax = 3.0;
const float yMin = 0.3;
const float zMax = 3.0;
const float zMin = 0.3;
const float Noise = 0.1;

const float thetaOffset = 330 *pi/180;
const float thetaA = 0 *pi/180 + thetaOffset;
const float thetaB = 120 *pi/180 + thetaOffset;
const float thetaC = 240 *pi/180 + thetaOffset;
const float r = 1.0;

float xVolt = 0.0;
float yVolt = 0.0;
float zVolt = 0.0;
float xDot = 0.0;
float yDot = 0.0;
float zDot = 0.0;

float aDot = 0.0;
float bDot = 0.0;
float cDot = 0.0;
float scale = 0.0;
unsigned int aVal = 0;
unsigned int bVal = 0;
unsigned int cVal = 0;

const int BaudRate = 9600;
int LoopDelay = 0;  // milliseconds (ms)
const int ConnectedDelay = 25;  // ms
const int DisconnectedDelay = 500;  // ms

char NewMessage = ' ';
unsigned long Timeout = 0;	// ms
unsigned long TimeoutDelay = 1000;  // ms

void setup()
{
  pinMode(x1Pin, INPUT);
  pinMode(y1Pin, INPUT);
  pinMode(x2Pin, INPUT);
  // pinMode(y2Pin, INPUT);
  pinMode(ConnectionLEDPin, OUTPUT);
  digitalWrite(ConnectionLEDPin, LOW);
  
  Serial1.begin(BaudRate);

  SetZero();
}

void loop()
{
  UpdateLED();
  ReadVolt();
  VoltToCart();
  CartToJoint();
  JointToDAC();
  TransmitDAC();
  delay(LoopDelay);
}





void SetZero()
{
  xZero = analogRead(x1Pin)/AnalogResolution*BoardVoltage;
  yZero = analogRead(y1Pin)/AnalogResolution*BoardVoltage;
  zZero = analogRead(x2Pin)/AnalogResolution*BoardVoltage;
}

void UpdateLED()
{
  if (CheckForMessage() == 'G')
  {
    digitalWrite(ConnectionLEDPin, HIGH);
    LoopDelay = ConnectedDelay;
  }
  else if(millis() < Timeout)
  {
    analogWrite(ConnectionLEDPin, 255 * (Timeout - millis()) / TimeoutDelay);
  }
  else
  {
    digitalWrite(ConnectionLEDPin, LOW);
    LoopDelay = DisconnectedDelay;
  }
}

char CheckForMessage()
{
  char NewMessage = ' ';

  while (Serial1.available() > 0)
  {
    NewMessage = Serial1.read();
    Timeout = millis() + TimeoutDelay;
  }

  return NewMessage;
}

void ReadVolt()
{
  xVolt = analogRead(x1Pin)/AnalogResolution*BoardVoltage;
  yVolt = analogRead(y1Pin)/AnalogResolution*BoardVoltage;
  zVolt = analogRead(x2Pin)/AnalogResolution*BoardVoltage;
}

void VoltToCart()
{
  xDot = (xVolt > xZero)?(xVolt - xZero)/(xMax - xZero)*(-1.0):(xVolt - xZero)/(xZero - xMin)*(-1.0);
  yDot = (yVolt > yZero)?(yVolt - yZero)/(yMax - yZero)*(-1.0):(yVolt - yZero)/(yZero - yMin)*(-1.0);
  zDot = (zVolt > zZero)?(zVolt - zZero)/(zMax - zZero)*(-1.0):(zVolt - zZero)/(zZero - zMin)*(-1.0);
  
  xDot = (abs(xDot) < Noise)?0.0:(xDot - Noise)/(1.0 - Noise);
  yDot = (abs(yDot) < Noise)?0.0:(yDot - Noise)/(1.0 - Noise);
  zDot = (abs(zDot) < Noise)?0.0:(zDot - Noise)/(1.0 - Noise);
}

void CartToJoint()
{
  aDot = xDot*sin(thetaA) + yDot*cos(thetaA) + zDot*r;
  bDot = xDot*sin(thetaB) + yDot*cos(thetaB) + zDot*r;
  cDot = xDot*sin(thetaC) + yDot*cos(thetaC) + zDot*r;

  scale = max(abs(aDot), max(abs(bDot), abs(cDot)));
  
  if(scale > 1)
  {
    aDot = aDot/scale;
    bDot = bDot/scale;
    cDot = cDot/scale;
  }
  
  // This provides an exponential speed response (as opposed to linear)
  // aDot = aDot*abs(aDot);
  // bDot = bDot*abs(bDot);
  // cDot = cDot*abs(cDot);
}

void JointToDAC()
{
  aVal = (aDot + 1.0)*DACResolution/2;
  bVal = (bDot + 1.0)*DACResolution/2;
  cVal = (cDot + 1.0)*DACResolution/2;
}

void TransmitDAC()
{
  Serial1.write('A');
  Serial1.write(aVal);
  Serial1.write('B');
  Serial1.write(bVal);
  Serial1.write('C');
  Serial1.write(cVal);
  Serial1.write('Z');
}
